/**
 * Wraps the iScroll object, providing fix() workaround for content scrolling too high
 * @param nodeId id of scroller DOM element
 * @constructor
 */
function ScrollWrapper(nodeId){
    /*jshint newcap:false */
    this.lastScrollY = 0;
    this.offset = 50;
    this.node = document.getElementById(nodeId);
    this.iScroll = new iScroll(nodeId);
}

/**
 * Limits maximum scrolling offset
 */
ScrollWrapper.prototype.fix = function(){
    /*
    if (this.iScroll.maxScrollY !== this.iScroll.lastScrollY) {
        this.iScroll.maxScrollY += this.node.offsetHeight - this.offset;
        this.lastScrollY = this.iScroll.maxScrollY;
    }
    */
};



