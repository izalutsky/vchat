/**
 * Main view of Dashboard entity
 * @type {*}
 */
var BoardView = Backbone.View.extend({
    initialize: function()
    {
        this.el = $("#text");
        this.model.on("change", this.render, this);
        this.iscroll = new ScrollWrapper("board");
    },

    render: function(){
        this.el.html(this.model.get("boardInfo"));
        this.iscroll.fix();
    }
});