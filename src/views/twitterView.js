/**
 * Main view of Twitter entity as a list of tweets
 * @type {*}
 */
var TwitterView = Backbone.View.extend({
    initialize: function(){
        this.model.on("change", this.render, this);
        this.el = $("#tweets");
    },
    render: function(){
        var json = {
            tweets: this.model.get("tweets").toJSON()
        };
        var markup = templater.tweets(json);
        this.el.html(markup);
        /*jshint newcap:false */
       var scroll = new iScroll("twitter");
    }
});
