
vchat.loader = new vchat.ScriptLoader();
var loader = vchat.loader;

var scriptUrl = "./js/main.js";
loader.require(scriptUrl);

function restoreLastLogin(){
    var input = document.getElementById("userName");
    var lastUserName = sessionStorage.getItem("vchat.lastUserName");
    if (lastUserName){
        input.value = lastUserName;
    }
}

function bindLogin(){
    var userName = input.value;
    if (userName !== "") {
        sendLoginRequest(userName);
    } else {
        console.log("Empty username");
        //TODO proper userName validation with UI message
    }
}

// handling submit events
var input = document.getElementById("userName");
var button = document.getElementById("enterButton");
button.addEventListener("click", bindLogin);
input.addEventListener("keydown", function(e){
   if (e.keyCode === 13) {
       bindLogin();
   }
});

restoreLastLogin();



function sendLoginRequest(userName){
    var url = "http://vapes.ws/videochat/mobile/login.php";
    var encodedName = encodeURIComponent(userName);
    var data = "userName=" + encodedName;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function(){
        if (xhr.readyState === 4 && xhr.status === 200) {
            handleLoginResponse(xhr.responseText);
        }
        //TODO xhr error handling
    };
    xhr.send(data);
}

function handleLoginResponse(text) {
    var data = JSON.parse(text);
    var lastUserName = sessionStorage.getItem("vchat.lastUserName");
    if (data.isValid || (data.name === lastUserName && data.isActive)) {
        sessionStorage.setItem("vchat.lastUserName", data.name);
        vchat.userName = data.name;
        loader.require(scriptUrl, function(){
            launchApp();
        });
    } else {
        //TODO login error handling
        console.log("some error");
    }
}

function launchApp(){
    var loginScreen = document.getElementById("login");
    loginScreen.setAttribute("style", "display: none");
}



