/**
 * Model of Dashboard entity
 * @type {*}
 */
var BoardModel = Backbone.Model.extend({
    defaults: {
        boardInfo: ''
    },

    initialize: function()
    {
        var that = this;
        $.get(
            "http://vapes.ws/videochat/mobile/readboard.php",
            function(data)
            {
                console.log(data);
                that.set({
                    boardInfo: data
                });
            }
        );
    }
});