/**
 * Model of Geolocation entity
 * @type {*}
 */
var LocationModel = Backbone.Model.extend({
    defaults: {
        url: "http://vapes.ws/videochat/mobile/readlatlng.php",
        lat: 0,
        lng: 0
    },
    initialize: function(){
        var that = this;
        $.getJSON(
            that.get("url"),
            function(data){
                if (data){
                    that.set({
                        lat: data.lat,
                        lng: data.lng
                    });
                }
            }
        );
    }
});