/**
 * Model of a single tweet
 * @type {*}
 */
var TweetModel = Backbone.Model.extend({
    defaults: {
        userName: "",
        text: "",
        picUrl: ""
    },
    initialize: function(){

    }
});

/**
 * Collection of tweets
 * @type {*}
 */
var TweetList = Backbone.Collection.extend({
    model: TweetModel
});

/* TODO refactor URLs to baseUrl */

/**
 * Model of Twitter entity
 * @type {*}
 */
var TwitterModel = Backbone.Model.extend({
    defaults: {
        timeout: 1000 * 60,
        tagUrl: "http://vapes.ws/videochat/mobile/readtweettag.php",
        queryUrl: "http://vapes.ws/videochat/mobile/tweetSearch.php?act=search&q=",
        tweets: new TweetList()
    },
    initialize: function(){
        var that = this;
        that.get("tweets").on("change", function(){        // passing collection change event to current model
            that.trigger("change");
        });
        this.startPoll();
    },
    startPoll: function(){
        var that = this;
        that.queryTag();
        setTimeout(function(){
            that.startPoll();
        }, that.get("timeout"));
    },
    queryTag: function(){
        var model = this;
        $.getJSON(model.get("tagUrl"), function(data){
            var query = data.tag;
            model.queryTweets(query);
        });
    },
    queryTweets: function(query){
        var model = this;
        $.getJSON(model.get("queryUrl") + query, function(response){
            model.addTweets(response.results);
        });
    },
    addTweets: function(tweets){
        var tweetModels = _.map(tweets, function(tweet){
            return new TweetModel({
                userName: tweet.from_user,
                text: tweet.text,
                picUrl: tweet.profile_image_url
            });
        });
        var collection = this.get("tweets");
        collection.reset(tweetModels);
        this.trigger("change");
    }
});