/**
 * Model of a single chat message
 * @type {*}
 */
var ChatMessageModel = Backbone.Model.extend({
    defaults: {
        userName: "",
        text: ""
    },
    initialize: function(){

    }
});

/**
 * Collection of tweets
 * @type {*}
 */
var ChatList = Backbone.Collection.extend({
    model: ChatMessageModel
});

/**
 * Model of Chat entity
 * @type {*}
 */

var ChatModel = Backbone.Model.extend({
    defaults: {
        timeout: 1000 * 2,
        chatUrl: 'http://vapes.ws/videochat/mobile/readchatmessages.php',
        chatText: '',
        chats: new ChatList()
    },

    initialize: function()
    {
        var that = this;
        that.startPoll();
    },

    startPoll: function(){
        var that = this;
        that.getMessages();
        setTimeout(function(){
            that.startPoll();
        }, that.get("timeout"));
    },

    getMessages: function()
    {
        var model = this;
        console.log(model.get("chatUrl"));
        $.getJSON(model.get("chatUrl"), function(data){
            console.log(data);
            model.addMessages(data);
        });
    },

    addMessages: function(chats)
    {
        var chatMessageModels = _.map(chats, function(chat){
            console.log(chat.userName);
            return new ChatMessageModel({
                userName: chat.userName,
                text: chat.userMessage
            });
        });
        var collection = this.get("chats");
        collection.reset(chatMessageModels);
        this.trigger("change");
    }
});