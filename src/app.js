var location = new LocationModel();
var map = new LocationView({ model: location });

var boardModel = new BoardModel();
var borderView = new BoardView({ model: boardModel });

var chatModel = new ChatModel();
var chatView = new ChatView({ model: chatModel });

var twitter = new TwitterModel();
var twitterView = new TwitterView({model: twitter});


var PageRouter = Backbone.Router.extend({
    initialize: function(){
        var that = this;
        that.currentPageName = null;
        that.pages = {};
        that.links = {};
        $("nav a").each(function(){
            var link = $(this);
            var pageName = link.attr("href").slice(1);
            that.links[pageName] = link;
            that.pages[pageName] = $("#" + pageName);
        });
    },
    routes: {
        ":pageName": "pageRoute"
    },
    pageRoute: function(pageName) {

        var lastPage = this.currentPageName;
        if (lastPage) {
            this.pages[lastPage].css("visibility", "hidden");
            this.links[lastPage].removeClass("selected");
        }
        if (this.pages[pageName]) {
            this.pages[pageName].css("visibility", "visible");
            this.links[pageName].addClass("selected");
            this.currentPageName = pageName;
        } else {
            console.log("No such page: " + pageName);
        }
    }
});

var router = new PageRouter();
Backbone.history.start();
router.navigate("chat", {
    replace: true,
    trigger: true
});
