
(function(){

    "use strict";

    if (!window.vchat) {
        window.vchat = {};
    }
    var vchat = window.vchat;

/**
 * Loads scripts dynamically by given url
 * @constructor
 */
vchat.ScriptLoader = function(){
    this.scripts = {};
};

/**
 * Loads the script and fires all callbacks as soon as it's ready
 * @param url
 * @param onReadyCallback
 */
vchat.ScriptLoader.prototype.require = function(url, onReadyCallback) {
    var script = this.scripts[url];
    if (!script) {
        this.scripts[url] = new ScriptDescriptor(url, onReadyCallback);
    } else if (script.state !== "ready") {
        script.addCallback(onReadyCallback);
    } else {
        onReadyCallback();
    }
};

/**
 * Contains various info about the script. Executes callbacks on script load.
 * @param url
 * @param firstCallback
 * @constructor
 */
function ScriptDescriptor(url, firstCallback){
    var that = this;
    this.state = "loading";
    this.callbacks = [];
    this.addCallback(firstCallback);
    this.script = document.createElement("script");
    this.script.type = "text/javascript";
    this.script.onload = function(){
        that.onReady();
    };
    this.script.src = url;
    document.getElementsByTagName("head")[0].appendChild(this.script);
}

/**
 * Adds callback to queue
 * @param callback
 */
ScriptDescriptor.prototype.addCallback = function(callback){
    if (typeof callback === "function") {
        this.callbacks.push(callback);
    }
};

/**
 * Changes state to ready and fires callbacks
 */
ScriptDescriptor.prototype.onReady = function(){
    this.state = "ready";
    for (var i = 0; i < this.callbacks.length; i+=1) {
        this.callbacks[i]();
    }
    this.callbacks = [];
};


vchat.loader = new vchat.ScriptLoader();
var loader = vchat.loader;

var scriptUrl = "./js/main.js";
loader.require(scriptUrl);

function restoreLastLogin(){
    var input = document.getElementById("userName");
    var lastUserName = sessionStorage.getItem("vchat.lastUserName");
    if (lastUserName){
        input.value = lastUserName;
    }
}

function bindLogin(){
    var userName = input.value;
    if (userName !== "") {
        sendLoginRequest(userName);
    } else {
        console.log("Empty username");
        //TODO proper userName validation with UI message
    }
}

// handling submit events
var input = document.getElementById("userName");
var button = document.getElementById("enterButton");
button.addEventListener("click", bindLogin);
input.addEventListener("keydown", function(e){
   if (e.keyCode === 13) {
       bindLogin();
   }
});

restoreLastLogin();



function sendLoginRequest(userName){
    var url = "http://vapes.ws/videochat/mobile/login.php";
    var encodedName = encodeURIComponent(userName);
    var data = "userName=" + encodedName;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function(){
        if (xhr.readyState === 4 && xhr.status === 200) {
            handleLoginResponse(xhr.responseText);
        }
        //TODO xhr error handling
    };
    xhr.send(data);
}

function handleLoginResponse(text) {
    var data = JSON.parse(text);
    var lastUserName = sessionStorage.getItem("vchat.lastUserName");
    if (data.isValid || (data.name === lastUserName && data.isActive)) {
        sessionStorage.setItem("vchat.lastUserName", data.name);
        vchat.userName = data.name;
        loader.require(scriptUrl, function(){
            launchApp();
        });
    } else {
        //TODO login error handling
        console.log("some error");
    }
}

function launchApp(){
    var loginScreen = document.getElementById("login");
    loginScreen.setAttribute("style", "display: none");
}





}());