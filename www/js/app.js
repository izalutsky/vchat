
(function(){

    "use strict";

    if (!window.vchat) {
        window.vchat = {};
    }
    var vchat = window.vchat;

/**
 * Wraps the iScroll object, providing fix() workaround for content scrolling too high
 * @param nodeId id of scroller DOM element
 * @constructor
 */
function ScrollWrapper(nodeId){
    /*jshint newcap:false */
    this.lastScrollY = 0;
    this.offset = 50;
    this.node = document.getElementById(nodeId);
    this.iScroll = new iScroll(nodeId);
}

/**
 * Limits maximum scrolling offset
 */
ScrollWrapper.prototype.fix = function(){
    /*
    if (this.iScroll.maxScrollY !== this.iScroll.lastScrollY) {
        this.iScroll.maxScrollY += this.node.offsetHeight - this.offset;
        this.lastScrollY = this.iScroll.maxScrollY;
    }
    */
};




/**
 * Creates an object with precompiled templating methods
 * @param {String[]} names Array of template names
 * @constructor
 */
function Templater(){
    var that = this;
    var templates = $("[data-template-name]");
    templates.each(function(){
        var element = $(this);
        var name = element.attr("data-template-name");
        var source = element.html();
        that[name] = Handlebars.compile(source);
    });
}
/*
    Application start code. Exeduted before creation of any models or views
*/

var templater = new Templater();
/**
 * Model of Dashboard entity
 * @type {*}
 */
var BoardModel = Backbone.Model.extend({
    defaults: {
        boardInfo: ''
    },

    initialize: function()
    {
        var that = this;
        $.get(
            "http://vapes.ws/videochat/mobile/readboard.php",
            function(data)
            {
                console.log(data);
                that.set({
                    boardInfo: data
                });
            }
        );
    }
});
/**
 * Model of a single chat message
 * @type {*}
 */
var ChatMessageModel = Backbone.Model.extend({
    defaults: {
        userName: "",
        text: ""
    },
    initialize: function(){

    }
});

/**
 * Collection of tweets
 * @type {*}
 */
var ChatList = Backbone.Collection.extend({
    model: ChatMessageModel
});

/**
 * Model of Chat entity
 * @type {*}
 */

var ChatModel = Backbone.Model.extend({
    defaults: {
        timeout: 1000 * 2,
        chatUrl: 'http://vapes.ws/videochat/mobile/readchatmessages.php',
        chatText: '',
        chats: new ChatList()
    },

    initialize: function()
    {
        var that = this;
        that.startPoll();
    },

    startPoll: function(){
        var that = this;
        that.getMessages();
        setTimeout(function(){
            that.startPoll();
        }, that.get("timeout"));
    },

    getMessages: function()
    {
        var model = this;
        console.log(model.get("chatUrl"));
        $.getJSON(model.get("chatUrl"), function(data){
            console.log(data);
            model.addMessages(data);
        });
    },

    addMessages: function(chats)
    {
        var chatMessageModels = _.map(chats, function(chat){
            console.log(chat.userName);
            return new ChatMessageModel({
                userName: chat.userName,
                text: chat.userMessage
            });
        });
        var collection = this.get("chats");
        collection.reset(chatMessageModels);
        this.trigger("change");
    }
});
/**
 * Model of Geolocation entity
 * @type {*}
 */
var LocationModel = Backbone.Model.extend({
    defaults: {
        url: "http://vapes.ws/videochat/mobile/readlatlng.php",
        lat: 0,
        lng: 0
    },
    initialize: function(){
        var that = this;
        $.getJSON(
            that.get("url"),
            function(data){
                if (data){
                    that.set({
                        lat: data.lat,
                        lng: data.lng
                    });
                }
            }
        );
    }
});
/**
 * Model of a single tweet
 * @type {*}
 */
var TweetModel = Backbone.Model.extend({
    defaults: {
        userName: "",
        text: "",
        picUrl: ""
    },
    initialize: function(){

    }
});

/**
 * Collection of tweets
 * @type {*}
 */
var TweetList = Backbone.Collection.extend({
    model: TweetModel
});

/* TODO refactor URLs to baseUrl */

/**
 * Model of Twitter entity
 * @type {*}
 */
var TwitterModel = Backbone.Model.extend({
    defaults: {
        timeout: 1000 * 60,
        tagUrl: "http://vapes.ws/videochat/mobile/readtweettag.php",
        queryUrl: "http://vapes.ws/videochat/mobile/tweetSearch.php?act=search&q=",
        tweets: new TweetList()
    },
    initialize: function(){
        var that = this;
        that.get("tweets").on("change", function(){        // passing collection change event to current model
            that.trigger("change");
        });
        this.startPoll();
    },
    startPoll: function(){
        var that = this;
        that.queryTag();
        setTimeout(function(){
            that.startPoll();
        }, that.get("timeout"));
    },
    queryTag: function(){
        var model = this;
        $.getJSON(model.get("tagUrl"), function(data){
            var query = data.tag;
            model.queryTweets(query);
        });
    },
    queryTweets: function(query){
        var model = this;
        $.getJSON(model.get("queryUrl") + query, function(response){
            model.addTweets(response.results);
        });
    },
    addTweets: function(tweets){
        var tweetModels = _.map(tweets, function(tweet){
            return new TweetModel({
                userName: tweet.from_user,
                text: tweet.text,
                picUrl: tweet.profile_image_url
            });
        });
        var collection = this.get("tweets");
        collection.reset(tweetModels);
        this.trigger("change");
    }
});

//TODO implement videochat model

/**
 * Main view of Dashboard entity
 * @type {*}
 */
var BoardView = Backbone.View.extend({
    initialize: function()
    {
        this.el = $("#text");
        this.model.on("change", this.render, this);
        this.iscroll = new ScrollWrapper("board");
    },

    render: function(){
        this.el.html(this.model.get("boardInfo"));
        this.iscroll.fix();
    }
});

//TODO implement chat and message views

var ChatView = Backbone.View.extend({

    el: $("#chat"),

    initialize: function()
    {
        this.userMessage = $("#userMessage");
        //this.el = $("#chat");
        //this.el2 = $("#chatMessages2");

        this.model.on("change", this.render, this);
    },

    events:
    {
        "click #sendMessage": "onSendMessage"
    },

    render: function(){
        var json = {
            chats: this.model.get("chats").toJSON()
        };
        var chatM = templater.chats(json);
        //this.el.children("#chatMessages").html(chatM);
        var messages = this.$("#chatMessages");
        messages.html(chatM);
        //this.el2.html(chatM);
        /*jshint newcap:false */
        var scroll = new iScroll("chatMessages");
        var height = messages.height();
        scroll.pageY = height;
        //var scroll2 = new iScroll("chatMessages2");
    },

    onSendMessage: function()
    {
        var uName = window.vchat.userName;
        var uMessage = this.userMessage.val().trim();


        console.log(uName);
        console.log(uMessage);

        $.post('http://vapes.ws/videochat/mobile/server.php', {user: uName, msg: uMessage}, function(response){
            console.log("mess send");
            $("#userMessage").val("");
        });
    }
});

/**
 * Main view of Geolocation entity as a map
 * @type {*}
 */
var LocationView = Backbone.View.extend({
    initialize: function(){
        this.leafletScriptPath = "./leaflet/leaflet.js";
        vchat.loader.require(this.leafletScriptPath);
        this.el = $("#map");
        this.minZoom = 3;
        this.maxZoom = 18;
        this.initialZoom = 13;
        this.model.on("change", this.render, this);
    },
    render: function(){
        var view = this;
        // setting fixed height to map container due to Leaflet requirements
        this.el.height(this.el.parent().height());
        // map needs Leaflet script to be loaded before rendering
        vchat.loader.require(this.leafletScriptPath, function(){
            var map = new L.Map('map');

            var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
            var osmAttrib = 'Map data © OpenStreetMap contributors';
            var osm = new L.TileLayer(osmUrl, {
                minZoom: view.minZoom,
                maxZoom: view.maxZoom,
                attribution: osmAttrib
            });

            var latitude = view.model.get("lat");
            var longitude = view.model.get("lng");

            map.setView(new L.LatLng(latitude, longitude), view.initialZoom);
            map.addLayer(osm);
            L.marker([latitude, longitude]).addTo(map);
        });
    }
});
/**
 * Main view of Twitter entity as a list of tweets
 * @type {*}
 */
var TwitterView = Backbone.View.extend({
    initialize: function(){
        this.model.on("change", this.render, this);
        this.el = $("#tweets");
    },
    render: function(){
        var json = {
            tweets: this.model.get("tweets").toJSON()
        };
        var markup = templater.tweets(json);
        this.el.html(markup);
        /*jshint newcap:false */
       var scroll = new iScroll("twitter");
    }
});


//TODO implement video chat view with HTML5 Video

var location = new LocationModel();
var map = new LocationView({ model: location });

var boardModel = new BoardModel();
var borderView = new BoardView({ model: boardModel });

var chatModel = new ChatModel();
var chatView = new ChatView({ model: chatModel });

var twitter = new TwitterModel();
var twitterView = new TwitterView({model: twitter});


var PageRouter = Backbone.Router.extend({
    initialize: function(){
        var that = this;
        that.currentPageName = null;
        that.pages = {};
        that.links = {};
        $("nav a").each(function(){
            var link = $(this);
            var pageName = link.attr("href").slice(1);
            that.links[pageName] = link;
            that.pages[pageName] = $("#" + pageName);
        });
    },
    routes: {
        ":pageName": "pageRoute"
    },
    pageRoute: function(pageName) {

        var lastPage = this.currentPageName;
        if (lastPage) {
            this.pages[lastPage].css("visibility", "hidden");
            this.links[lastPage].removeClass("selected");
        }
        if (this.pages[pageName]) {
            this.pages[pageName].css("visibility", "visible");
            this.links[pageName].addClass("selected");
            this.currentPageName = pageName;
        } else {
            console.log("No such page: " + pageName);
        }
    }
});

var router = new PageRouter();
Backbone.history.start();
router.navigate("chat", {
    replace: true,
    trigger: true
});


}());